#!/bin/bash
#
# pulseaudio_record_from_anything - record from all sources with pulseaudio
#
# Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.

set -e

command -v /usr/bin/parec > /dev/null || { echo "parec missing, install pulseaudio-utils" 1>&2; exit 1; }
command -v /usr/bin/oggenc > /dev/null || { echo "oggenc missing, install vorbis-tools" 1>&2; exit 1; }

if pactl list short sinks | grep -q module-null-sink;
then
  echo "The null sink is already loaded, bailing out." 1>&2
  exit 1
fi

# Load the null sink.
# It'll act as a "kitchen sink"... I'll show myself out.
PA_IDS=$(pactl load-module module-null-sink)
trap 'exit 1' INT
trap 'trap INT; for pa_id in $PA_IDS; do pactl unload-module "$pa_id"; done' EXIT

# Record from everything by redirecting any source to the null sink.
SOURCES=$(pactl list short sources | cut -f 2 | grep -v '^null\.monitor$')
for source in $SOURCES;
do
  MODULE_ID=$(pactl load-module module-loopback latency_msec=1 source="$source" sink=null)
  PA_IDS="$MODULE_ID $PA_IDS"
done

# Listen to what you are recording.
# NOTE: beware of Larsen effect when using microphones.
#pactl load-module module-loopback source=null.monitor sink=@DEFAULT_SINK@

# Un-mute the sink.
pactl set-sink-mute null 0

# Record everything.
parec -d null.monitor | oggenc -b 192 -o "recording_output_$(date +"%F_%T").ogg" --raw -
